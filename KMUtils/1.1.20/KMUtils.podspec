Pod::Spec.new do |s|
s.name             = 'KMUtils'
s.version          = '1.1.20'
s.summary          = 'Utilities to use in all apps.'
s.description      = 'Utilities to use in all apps. EZApps'

s.homepage         = 'https://bitbucket.org/mabas2410/kmutils'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'Mauricio Ventura' => 'mv@ezappsnow.com' }
s.source           = { :git => 'https://mabas2410@bitbucket.org/mabas2410/kmutils.git', :tag => s.version.to_s }
s.social_media_url = 'https://twitter.com/RCI_SSVPMB'
s.ios.deployment_target = '9.0'
s.source_files = 'KMUtils/Classes/**/*'
end
