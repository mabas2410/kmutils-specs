#
# Be sure to run `pod lib lint KMUtils.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'KMUtils'
  s.version          = '1.0'
  s.summary          = 'Utilities to use in all apps.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC
  s.homepage         = 'https://bitbucket.org/mabas2410/kmutils'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Mauricio Ventura' => 'mabavepe2410@hotmail.com' }
  s.source           = { :git => 'https://mabas2410@bitbucket.org/mabas2410/kmutils.git', :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/RCI_SSVPMB'

  s.ios.deployment_target = '9.0'

  s.source_files = 'KMUtils/Classes/**/*'
  
  # s.resource_bundles = {
  #   'KMUtils' => ['KMUtils/Assets/*.png']
  # }

# s.public_header_files = 'KMUtils/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
# s.dependency 'AFNetworking'
end
